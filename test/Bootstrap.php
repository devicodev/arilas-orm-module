<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/26/13
 * Time: 6:28 PM
 */

namespace Arilas\ORMTest;

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

class Bootstrap
{
    protected static $serviceManager;
    protected static $config;
    protected static $bootstrap;

    public static function init()
    {
        date_default_timezone_set('UTC');
        // Load the user-defined test configuration file, if it exists; otherwise, load
        if (is_readable(__DIR__ . '/TestConfig.php')) {
            $testConfig = include __DIR__ . '/TestConfig.php';
        } else {
            $testConfig = include __DIR__ . '/TestConfig.php.dist';
        }

        $global = array_merge(
            $testConfig,
            include __DIR__ . '/test.config.php'
        );
        $serviceManager = new ServiceManager(
            new ServiceManagerConfig()
        );
        $serviceManager->setService('ApplicationConfig', $global);
        $serviceManager->get('ModuleManager')->loadModules();

        static::$serviceManager = $serviceManager;
        static::$config = $global;
    }

    /**
     * @return ServiceManager
     */
    public static function getServiceManager()
    {
        return static::$serviceManager;
    }

    public static function getConfig()
    {
        return static::$config;
    }
}

Bootstrap::init();
