<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/26/13
 * Time: 6:47 PM
 */

namespace Arilas\ORMTest\Repository;

use Arilas\ORM\EntityManager;
use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Test\Test;
use PHPUnit_Framework_TestCase;
use Zend\EventManager\Event;

class AbstractRepositoryTest extends PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testCreate()
    {
        self::$orm->getEventManager()->attach(EntityManager::PRE_INSERT_EVENT, function (Event $e) {
            $this->assertInstanceOf(Test::class, $e->getParam('entity'));
        });
        $testEntity = new Test();
        $testEntity->setValue("test");
        $testEntity->id = 1;
        self::$orm->commit($testEntity);

        self::$orm->clear(Test::class);

        $testEntity->setValue('test@name.ua');
        self::$orm->commit($testEntity);
    }

    /**
     * @depends testCreate
     */
    public function testFetch()
    {
        /** @var Test $test */
        $test = self::$orm->find(
            Test::class,
            1
        );
        $this->assertInstanceOf(
            Test::class,
            $test
        );
        $test->setValue("newValue");
        self::$orm->commit($test);
        $this->assertInstanceOf(
            Test::class,
            self::$testRepository->findOneBy([
                'value' => 'newValue'
            ])
        );
        $this->assertArrayHasKey(
            1,
            self::$testRepository->findAll()
        );
    }

    /**
     * @depends testCreate
     */
    public function testRemove()
    {
        /** @var Test $test */
        $test = self::$orm->find(
            Test::class,
            1
        );
        self::$orm->remove($test);
        $this->assertTrue(count(self::$testRepository->findAll()) == 0);
    }
}
