<?php
/**
 * User: krona
 * Date: 11/14/14
 * Time: 6:44 PM
 */

namespace Arilas\ORM\Mapping;

/**
 * Class Connect
 * @package Arilas\ORM\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Connect
{
    /**
     * Class Name
     * @var string
     */
    public $targetEntity;

    /** @var string */
    public $mappedBy;

    /** @var bool */
    public $useIdentity = false;

    /** @var  string */
    public $identityField;
}