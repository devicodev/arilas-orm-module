<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/26/13
 * Time: 5:11 PM
 */

namespace Arilas\ORM\Service;

use Arilas\ORM\EntityManager;
use Zend\EventManager\EventManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{

    /** @var  EntityManager */
    protected $instance;

    /**
     * Create service
     *
     * @param  ServiceLocatorInterface $serviceLocator
     * @return EntityManager
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        if (is_null($this->instance)) {
            $config = $serviceLocator->get('Config');
            /** @var EventManager $eventManager */
            $eventManager = $serviceLocator->get('EventManager');
            $connectionFactory = new ConnectionFactory($serviceLocator);
            $this->instance = new EntityManager($eventManager, $connectionFactory, $config);
        }

        return $this->instance;
    }
}
